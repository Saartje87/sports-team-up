const webpack = require('webpack');
const Dotenv = require('dotenv-webpack');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');

const { config, PATHS } = require('./webpack-base.config');

module.exports = {
  ...config,
  devtool: 'eval-source-map',
  devServer: {
    contentBase: PATHS.public,
    disableHostCheck: true,
    hot: true,
    noInfo: false,
    host: '0.0.0.0',
    port: 8000,
  },
  module: {
    ...config.module,
    rules: [
      ...config.module.rules,
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader?sourceMap', 'postcss-loader'],
      },
    ],
  },
  plugins: [
    new ReactRefreshWebpackPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new Dotenv({
      path: './.env.local',
    }),
    ...config.plugins,
  ],
};
