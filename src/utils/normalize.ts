type NormalizeReturn<T> = { [key: string]: T };

/**
 * Creates a new object of an array mapped by specified key
 *
 * const normalizer = createNormalizer<Entity>('key')\
 * normalizer([{ key: '1a' }]); // { '1a': { key: '1a' } }
 */
export const createNormalizer = <T extends NormalizeReturn<any>>(key: keyof T) => (from: T[]) => {
  const target: NormalizeReturn<T> = {};

  from.forEach(entity => {
    target[entity[key]] = entity;
  });

  return target;
};
