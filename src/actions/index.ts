import { UserActions } from './userActions';

export type AppActions = UserActions;
