import { AppActions } from 'actions';
import { AppState } from 'reducers';
import { Dispatch } from 'redux';
import { ThunkAction as ReduxThunkAction } from 'redux-thunk';

/**
 * Action helpers
 */

export interface Action<Type extends string> {
  type: Type;
}

export interface ActionWithPayload<Type extends string, Payload, Meta> extends Action<Type> {
  type: Type;
  payload: Payload;
  meta?: Meta;
}

export const createAction = <Type extends string>(type: Type): Action<Type> => ({
  type,
});

export const createActionWithPayload = <Type extends string, Payload, Meta = {}>(
  type: Type,
  payload: Payload,
  meta?: Meta,
): ActionWithPayload<Type, Payload, Meta> => ({
  type,
  payload,
  meta,
});

interface CreateApiOptions {
  method: 'GET' | 'PUT' | 'POST' | 'PATCH' | 'DELETE';
  path: string;
  onSuccess?: (dispatch: Dispatch, response: any) => void;
  onError?: (dispatch: Dispatch, error: Error) => void;
}

export function createApiAction<T extends string, P>(type: T, api: CreateApiOptions, payload?: P) {
  return createActionWithPayload(type, payload, {
    api,
  });
}

export type ThunkAction<R = void> = ReduxThunkAction<R, AppState, void, AppActions>;
