import { User } from 'types/user';
import { createAction, createActionWithPayload, createApiAction } from './helpers';

// Async actions
interface SignIn {
  email: string;
  password: string;
  rememberMe?: boolean;
}

export const signIn = (userData: SignIn) =>
  createApiAction(
    'LOGIN_USER',
    {
      method: 'POST',
      path: '/user/login',
      onSuccess: (dispatch, user: User) => dispatch(storeUser(user)),
    },
    userData,
  );

export const logout = () =>
  createApiAction('LOGIN_USER', {
    method: 'POST',
    path: '/user/logout',
    onSuccess: (dispatch) => dispatch(clearUser()),
    // TODO Remove onError when server contains logout endpoint.
    onError: (dispatch) => dispatch(clearUser()),
  });

// Sync actions
export const storeUser = (user: User) => createActionWithPayload('@user/STORE_USER', user);

export const clearUser = () => createAction('@user/CLEAR');

export type UserActions = ReturnType<typeof storeUser | typeof clearUser>;
