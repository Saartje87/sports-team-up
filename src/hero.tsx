import React, { useState } from 'react';

const ListScreen = () => {
  const [selected, setSelected] = useState<string | null>(null);

  return (
    <>
      <h1>List</h1>
      <ul>
        {['A', 'B'].map(id => (
          <li key={id} onClick={() => setSelected(id)}>
            <Hero
              active={selected === id}
              id={`avatar-${id}`}
              render={ref => <img ref={ref} src="" />}
            />
            <span>Name {id}.</span>
          </li>
        ))}
      </ul>

      <Paper open={!!selected}>
        <DetailScreen id={selected} />
      </Paper>
    </>
  );
};

const DetailScreen = ({ id }: { id?: string }) => {
  return (
    <>
      <h1>{id}</h1>
      <Hero active={!!id} id={`avatar-${id}`} render={ref => <img ref={ref} src="" />} />
    </>
  );
};

const Woop = () => {
  return (
    <HeroProvider>
      <Hero render={ref => <img ref={ref} src="" />} />
      <HeroMask open={false} render={ref => <h1>Ponies</h1>} />
    </HeroProvider>
  );
};

// V2

// HERO === FLIP

const Overview = () => {
  const hero = useHero();

  // hero.from(id, ref) -> hero.to(id, ref)

  return (
    <ul>
      <li ref={element => hero.from('id-1', element)}>Title a</li>
      <li ref={element => hero.from('id-2', element)}>Title b</li>
    </ul>
  );
};
