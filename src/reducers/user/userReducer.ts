import { AppActions } from 'actions';
import { Reducer } from 'redux';
import { User } from 'types/user';

export type UserReducer = Partial<User>;

const initialState: UserReducer = {};

export const userReducer: Reducer<UserReducer, AppActions> = (state = initialState, action) => {
  switch (action.type) {
    case '@user/CLEAR':
      return initialState;

    case '@user/STORE_USER':
      return {
        ...state,
        ...action.payload,
      };

    default:
      return state;
  }
};
