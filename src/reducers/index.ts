import { UserReducer } from './user';

export { userReducer as user } from './user';

export interface AppState {
  user: UserReducer;
}
