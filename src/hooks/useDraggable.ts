import { useEffect } from 'react';

interface Position {
  x: number;
  y: number;
}

// function getPosition(element: HTMLElement): Position {
//   const { x, y } = element.getBoundingClientRect();

//   const scrollLeft = document.documentElement.scrollLeft;
//   const scrollTop = document.documentElement.scrollTop;

//   return {
//     x: x + scrollLeft,
//     y: y + scrollTop,
//   };
// }

function getEventPosition(event: MouseEvent | TouchEvent): Position {
  if ('touches' in event) {
    return {
      x: event.touches[0].pageX,
      y: event.touches[0].pageY,
    };
  }

  return { x: event.pageX, y: event.pageY };
}

interface UseDraggable {
  ref: React.RefObject<HTMLElement>;
  contain?: 'parent';
  axis?: 'both' | 'x' | 'y';
  onStart?: (Element: HTMLElement) => void;
  onMove?: (element: HTMLElement, x: number, y: number) => void;
  onEnd?: (element: HTMLElement, x: number, y: number) => void;
}

export function useDraggable({ ref, onStart, onMove, onEnd }: UseDraggable) {
  useEffect(() => {
    const element = ref.current;

    if (!element) {
      return;
    }

    function start(event: MouseEvent | TouchEvent) {
      if (!element) {
        return;
      }

      const startPosition = getEventPosition(event);
      let deltaX = 0;
      let deltaY = 0;

      function move(event: MouseEvent | TouchEvent) {
        if (!element) {
          return end();
        }

        const { x, y } = getEventPosition(event);
        deltaX = x - startPosition.x;
        deltaY = y - startPosition.y;

        if (onMove) {
          onMove(element, deltaX, deltaY);
        }
      }

      function end(event?: MouseEvent | TouchEvent) {
        if (Math.abs(deltaX) > 3 || Math.abs(deltaY) > 3) {
          if (event) {
            event.stopPropagation();
            event.preventDefault();
          }
        } else {
          document.removeEventListener('click', preventClick, { capture: true });
        }

        document.removeEventListener('mousemove', move);
        document.removeEventListener('mouseup', end);
        document.removeEventListener('touchmove', move);
        document.removeEventListener('touchend', end);
        document.removeEventListener('touchcancel', end);

        if (event) {
          switch (event.type) {
            case 'click':
            case 'touchend':
            case 'touchcancel':
              document.removeEventListener('click', preventClick, { capture: true });
          }
        }

        if (onEnd && element) {
          onEnd(element, deltaX, deltaY);
        }
      }

      function preventClick(event: MouseEvent) {
        event.preventDefault();
        event.stopPropagation();
        event.stopImmediatePropagation();

        document.removeEventListener('click', preventClick, { capture: true });
      }

      document.addEventListener('mousemove', move);
      document.addEventListener('mouseup', end);
      document.addEventListener('touchmove', move);
      document.addEventListener('touchend', end);
      document.addEventListener('touchcancel', end);
      document.addEventListener('click', preventClick, { capture: true });

      if (onStart) {
        onStart(element);
      }
    }

    element.addEventListener('mousedown', start);
    element.addEventListener('touchstart', start);

    return () => {
      element.removeEventListener('mousedown', start);
      element.removeEventListener('touchstart', start);
    };
  }, [ref.current, onStart, onMove, onEnd]);
}
