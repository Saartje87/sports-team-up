import { AppState } from 'reducers';

export const isAuthenticated = (state: AppState) => !!state.user.id;

export const getFirstName = (state: AppState) => state.user.firstName;
