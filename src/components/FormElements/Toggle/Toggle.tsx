import { Box, cx } from '@blockle/blocks';
import { FieldProps, useField } from '@blockle/form';
import { useDraggable } from 'hooks/useDraggable';
import React, { useRef } from 'react';
import './toggle.css';

const MAGIC_NUMBER = 31;

function clamp(a: number, b: number, c: number) {
  return Math.max(a, Math.min(b, c));
}

interface Props extends FieldProps<string> {
  checked?: boolean;
  name: string;
  onChange?: (value: boolean) => void;
  required?: boolean;
}

const Toggle = ({ checked, name, required }: Props) => {
  const { value, setValue } = useField<boolean>(name, {
    value: !!checked,
    validate: (value: boolean) => {
      return required && !value ? 'required' : null;
    },
  });
  const handlerRef = useRef<HTMLDivElement>(null);

  useDraggable({
    ref: handlerRef,
    onStart(element) {
      element.style.transition = 'none';
    },
    onMove(element, x) {
      const maxX =
        clamp(value ? -MAGIC_NUMBER : 0, value ? 0 : MAGIC_NUMBER, x) + (value ? MAGIC_NUMBER : 0);

      element.style.transform = `translateX(${maxX}px)`;
    },
    onEnd(element, x) {
      const maxX =
        clamp(value ? -MAGIC_NUMBER : 0, value ? 0 : MAGIC_NUMBER, x) + (value ? MAGIC_NUMBER : 0);

      element.style.transition = '';
      element.style.transform = '';

      console.log(Math.abs(x));

      if (Math.abs(x) > 5) {
        setValue(!value);
      }

      // setValue(maxX < MAGIC_NUMBER / 2 ? false : true);
    },
  });

  return (
    <Box
      className={cx('Toggle', value && 'is-checked')}
      position="relative"
      backgroundColor={value ? 'primary' : 'gray'}
      onClick={() => setValue(!value)}
      role="checkbox"
      aria-checked={value}
      tabIndex={0}
      onKeyUp={(event) => {
        if (event.key === ' ') {
          setValue(!value);
        }
      }}
    >
      <Box ref={handlerRef} className="Toggle-handler" backgroundColor="white" />
    </Box>
  );
};

export default Toggle;
