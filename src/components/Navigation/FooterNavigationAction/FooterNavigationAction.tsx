import { Icon, IconNames, RippleBox, Text } from '@blockle/blocks';
import { Route } from '@blockle/router';
import React from 'react';
import './footer-navigation-action.css';

interface Props {
  icon: IconNames;
  label: string;
  to: string;
}

const FooterNavigationAction = ({ icon, label, to }: Props) => {
  return (
    <Route
      path={to}
      exact={to === '/'}
      render={(match) => (
        <RippleBox
          component="a"
          href={`#${to}`}
          className={`FooterNavigationAction ${match ? 'is-active' : ''}`}
        >
          <Icon name={icon} label={label} color={match ? 'primary' : 'gray'} />
          <Text fontSize="xsmall" color="black">
            {label}
          </Text>
        </RippleBox>
      )}
    />
  );
};

export default FooterNavigationAction;
