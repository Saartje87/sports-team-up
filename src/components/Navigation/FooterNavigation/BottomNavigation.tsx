import React from 'react';

import './footer-navigation.css';

interface Props {
  children: React.ReactNode;
}

const FooterNavigation = ({ children }: Props) => (
  <div className="FooterNavigation" role="navigation">
    {children}
  </div>
);

export default FooterNavigation;
