import '@testing-library/jest-dom/extend-expect';
import React from 'react';
import { render, cleanup } from 'testUtils';

import Logo from './Logo';

afterEach(cleanup);

describe('Logo', () => {
  it('should render icon when a valid name is given', () => {
    const { getByAltText } = render(<Logo />);

    expect(getByAltText('Blockle logo')).toBeTruthy();
  });
});
