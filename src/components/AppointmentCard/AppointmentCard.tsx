import { Box, Card, Stack, Text } from '@blockle/blocks';
import React, { FC } from 'react';
import './appointment-card.css';

interface Props {
  appointmentId: string;
}

// TODO Move to date utils file
const formatMonth = new Intl.DateTimeFormat('nl-NL', { month: 'short' });
const formatWeekDay = new Intl.DateTimeFormat('nl-NL', { weekday: 'short' });
const formatTime = new Intl.DateTimeFormat('nl-NL', { hour: '2-digit', minute: '2-digit' });
const formatDate = new Intl.DateTimeFormat('nl-NL', {
  month: 'long',
  day: 'numeric',
  year: 'numeric',
  weekday: 'short',
});

const AppointmentCard: FC = () => {
  const startDate = new Date(Date.now());
  const stopDate = new Date(Date.now() + 3600000);

  return (
    <Card display="flex" shadow={1}>
      <Box
        className="AppointmentCard-Date"
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
        backgroundColor="primary"
        padding="small"
        style={{ textTransform: 'uppercase' }}
      >
        <Stack spacing="xsmall" align="center">
          <Text color="white">{formatWeekDay.format(startDate)}</Text>
          <Text color="white">{startDate.getDate()}</Text>
          <Text color="white">{formatMonth.format(startDate)}</Text>
        </Stack>
      </Box>
      <Box padding={['small', 'medium']}>
        <Stack spacing="xsmall">
          <Text color="black">Location</Text>
          <Text color="gray">
            From {formatTime.format(startDate)} till {formatTime.format(stopDate)}
          </Text>
          <Text color="gray">Participants: 3</Text>
        </Stack>
      </Box>
    </Card>
  );
};

export default AppointmentCard;
