import { Provider } from '@blockle/react-redux';
import { Router } from '@blockle/router';
import { persistor, store } from 'config/store';
import { createHashHistory } from 'history';
import React from 'react';
import { PersistGate } from 'redux-persist/integration/react';

type Props = {
  children: React.ReactNode;
};

const AppShell = ({ children }: Props) => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router history={createHashHistory()}>{children}</Router>
      </PersistGate>
    </Provider>
  );
};

export default AppShell;
