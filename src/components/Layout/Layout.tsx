import { cx } from '@blockle/blocks';
import React from 'react';
import './layout.css';

type Props = {
  children: React.ReactNode;
  fit?: boolean;
};

const Layout = ({ children, fit }: Props) => {
  return <div className={cx('Layout', fit && 'is-fit')}>{children}</div>;
};

export default Layout;
