import { Box, FlatButton, Panel, Stack, Text } from '@blockle/blocks';
import React, { useEffect, useState } from 'react';
import { Workbox } from 'workbox-window';

type State = 'idle' | 'activated' | 'waiting';

const ServiceWorker = () => {
  const [state, setState] = useState<State>('idle');
  const reload = () => window.location.reload();

  useEffect(() => {
    if (!('serviceWorker' in navigator) || module.hot) {
      return;
    }

    const wb = new Workbox('/sw.js');

    wb.addEventListener('activated', (event) => {
      if (event.isUpdate) {
        setState('activated');
      }
    });

    wb.addEventListener('waiting', () => {
      setState('waiting');
    });

    wb.register();
  }, []);

  if (state === 'activated') {
    return (
      <Panel
        open
        onRequestClose={reload}
        render={() => (
          <Box paddingX="gutter" paddingTop="large">
            <Stack spacing="large">
              <Text component="h2" fontSize="large">
                Update found
              </Text>
              <FlatButton onClick={reload}>Install</FlatButton>
            </Stack>
          </Box>
        )}
      />
    );
  }

  if (state === 'waiting') {
    console.log('Waiting for sw update');
  }

  return null;
};

export default ServiceWorker;
