import { Box, Button, FlatButton, IconButton, Stack, Text, TextField } from '@blockle/blocks';
import { useHistory } from '@blockle/router';
import React, { FC } from 'react';

const CreateAppointmentScreen: FC = () => {
  const { back } = useHistory();

  return (
    <>
      <Box display="flex" alignItems="center" padding="xsmall">
        <Box flexGrow={1}>
          <Text component="h1" textAlign="center">
            Add Appointment
          </Text>
        </Box>
        <IconButton name="cross" label="Close" onClick={back} />
      </Box>
      <Box flexGrow={1} padding="gutter">
        <Stack spacing="medium">
          <TextField name="when" label="When" />
          <TextField name="where" label="Where" />

          <Stack spacing="small">
            <Text color="black">Activities</Text>
            <Text color="gray">No activities selected</Text>
            <Box display="flex" justifyContent="flex-end">
              <FlatButton>Add activity</FlatButton>
            </Box>
          </Stack>

          <Box display="flex" justifyContent="flex-end">
            <Button type="submit">Submit</Button>
          </Box>
        </Stack>
      </Box>
    </>
  );
};

export default CreateAppointmentScreen;
