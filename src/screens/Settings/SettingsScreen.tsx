import { Box, FlatButton, Stack, Text } from '@blockle/blocks';
import { useDispatch } from '@blockle/react-redux';
import { clearUser } from 'actions/userActions';
import React from 'react';

const SettingsScreen = () => {
  const dispatch = useDispatch();

  return (
    <Box flexGrow={1} padding="gutter">
      <Stack spacing="medium">
        <Text component="h1" fontSize="xlarge">
          Settings
        </Text>

        <FlatButton onClick={() => dispatch(clearUser())}>Logout</FlatButton>
      </Stack>
    </Box>
  );
};

export default SettingsScreen;
