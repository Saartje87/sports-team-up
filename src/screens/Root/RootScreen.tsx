import { useSelector } from '@blockle/react-redux';
import ServiceWorker from 'components/ServiceWorker';
import React from 'react';
import ApplicationScreen from 'screens/Application';
import SignInScreen from 'screens/SignIn';
import { isAuthenticated } from 'selectors/userSelectors';
import './root-screen.css';

const RootScreen = () => {
  const authenticated = useSelector(isAuthenticated);

  return (
    <>
      {!authenticated ? <SignInScreen /> : <ApplicationScreen />}

      <ServiceWorker />
    </>
  );
};

export default RootScreen;
