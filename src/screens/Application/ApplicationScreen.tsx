import { Box } from '@blockle/blocks';
import { Route, RouteGroup } from '@blockle/router';
import Layout from 'components/Layout';
import { FooterNavigation, FooterNavigationAction } from 'components/Navigation';
import RouteTransition from 'components/RouteTransition';
import React from 'react';
import AgendaScreen from 'screens/Agenda';
import CreateAppointmentScreen from 'screens/CreateAppointment';
import SettingsScreen from 'screens/Settings/SettingsScreen';

const ApplicationScreen = () => {
  return (
    <>
      <Layout>
        <RouteGroup>
          <Route path={['/', '/appointment/add', '/appointment/:id']} exact>
            <AgendaScreen />
          </Route>
          <Route path="/settings">
            <SettingsScreen />
          </Route>
          <Route noMatch>
            <Box padding="gutter" flexGrow={1}>
              Oops
            </Box>
          </Route>
        </RouteGroup>

        <FooterNavigation>
          <FooterNavigationAction to="/" icon="briefcase" label="Agenda" />
          <FooterNavigationAction to="/settings" icon="cog" label="Settings" />
        </FooterNavigation>
      </Layout>

      <RouteTransition path="/appointment/add" effect="slideUp">
        <CreateAppointmentScreen />
      </RouteTransition>
    </>
  );
};

export default ApplicationScreen;
