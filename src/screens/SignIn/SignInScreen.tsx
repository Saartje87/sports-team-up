import { Box, Button, Stack, Text, TextField } from '@blockle/blocks';
import { Form } from '@blockle/form';
import { useDispatch } from '@blockle/react-redux';
import { storeUser } from 'actions/userActions';
import React, { useState } from 'react';
import './sign-in-screen.css';

interface FormData {
  email: string;
  password: string;
  rememberMe: boolean;
}

const SignInScreen = () => {
  const dispatch = useDispatch();
  const [errorCode, setErrorCode] = useState<string | null>(null);

  return (
    <Box
      width="full"
      height="full"
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
    >
      <Box className="SignInScreen-box">
        <Form
          onSubmit={async (data: FormData) => {
            setErrorCode(null);

            try {
              dispatch(
                storeUser({
                  id: '1x',
                  email: 'dummy@dummy',
                  firstName: 'Los',
                  lastName: 'Barros',
                }),
              );
              // await dispatch(signIn(data));
            } catch (e) {
              setErrorCode(e.error || 'Oops something went wrong');
            }
          }}
          render={({ submitting }) => (
            <Stack spacing="xlarge">
              <Stack spacing="small">
                <Text
                  component="h1"
                  fontWeight="bold"
                  fontSize="xlarge"
                  textAlign="center"
                  color="primary"
                >
                  Team Up
                </Text>
              </Stack>

              <Stack spacing="small">
                <TextField name="email" type="email" label="Email" required />
                <TextField name="password" type="password" label="Password" required />

                {errorCode && (
                  <Text textAlign="center" color="danger">
                    {errorCode}
                  </Text>
                )}

                <Box display="flex" justifyContent="flex-end">
                  <Button type="submit" disabled={submitting}>
                    Sign in
                  </Button>
                </Box>
              </Stack>
            </Stack>
          )}
        />
      </Box>
    </Box>
  );
};

export default SignInScreen;
