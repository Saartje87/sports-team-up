import { Box, Button, Icon, Paper, Stack, Text } from '@blockle/blocks';
import { useHistory } from '@blockle/router';
import AppointmentCard from 'components/AppointmentCard';
import React, { useState } from 'react';

const AgendaScreen = () => {
  const { push } = useHistory();
  const [fullCTA, setFullCTA] = useState(true);

  return (
    <Box
      height="full"
      width="full"
      display="flex"
      flexDirection="column"
      padding="gutter"
      flexGrow={1}
      overflow="auto"
      onScroll={(event) => {
        setFullCTA(event.currentTarget.scrollTop < 40);
      }}
    >
      <Stack spacing="medium">
        <Text component="h1" fontSize="xlarge">
          Agenda
        </Text>

        <AppointmentCard />
        <AppointmentCard />
        <AppointmentCard />
        <AppointmentCard />
        <AppointmentCard />
        <AppointmentCard />
        <AppointmentCard />
        <AppointmentCard />
        <AppointmentCard />
        <AppointmentCard />
        <AppointmentCard />
        <AppointmentCard />
        <AppointmentCard />
      </Stack>

      <Box
        position="fixed"
        display="flex"
        justifyContent="center"
        paddingBottom="gutter"
        style={{
          right: 18,
          bottom: 50,
        }}
      >
        <Paper
          open
          effect="slideUp"
          render={() => (
            <Button onClick={() => push('/appointment/add')}>
              {fullCTA ? 'Create appointment' : <Icon name="plus" label="Create appointment" />}
            </Button>
          )}
        />
      </Box>
    </Box>
  );
};

export default AgendaScreen;
