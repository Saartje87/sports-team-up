#!/bin/bash

# Check if two arguments are given
if [[ $# -ne 2 ]] ; then
    echo 'Arguments missing, shutting down...'
    exit 1
fi

BUILD_PATH=$1
PROJECT_ENVIRONMENT=$2
SHARED_ENVIRONMENT_PATH="/var/applications/control.blockle.app/$PROJECT_ENVIRONMENT/shared/.env"


###
### Pre checks
###
# Change directory to project path or exit if cd is not possible (to prevent execution in wrong directory).
cd "$PROJECT_PATH" || exit 1;

# Check if required directory is in place
if [ ! -f "$SHARED_ENVIRONMENT_PATH" ]; then
  echo "No environment file found: $SHARED_ENVIRONMENT_PATH..."
  exit 1
fi
echo "Deployment directory structure is good, continuing..."


###
### Link environment file
###
echo "Creating symlink for environment variable..."
# Create symlink to shared environment file
ln -s "$SHARED_ENVIRONMENT_PATH" "$BUILD_PATH/.env"
