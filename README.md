# Blocke Control Client

## Setup

```bash
yarn
```

## Build

```bash
yarn build:production
yarn build:develop
```

## Test

```bash
yarn test
yarn lint
```

## Layout

```tsx
<FullScreenLayout
  title={<Text component="h1">Welcome back</Text>}
  onClose={() => goBack()}
  icon="cross"
>
  <TextInput name="email" />
  <TextInput name="password" />

  <Button type="submit">Sign in</Button>
</FullScreenLayout>
```

```tsx
<ScreenLayout title="Settings"></ScreenLayout>
```

```tsx
<PanelLayout title="Add "></PanelLayout>
```
